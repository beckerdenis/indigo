#ifndef _AI_PLAYER_H_
#define _AI_PLAYER_H_

#include "player.h"

class Game;

class AIPlayer : public Player {

public:
    AIPlayer(Color color, Game & game) :
        Player(color), game(game) {}
    virtual ~AIPlayer() {}
    
    virtual void notify(Action & action) = 0;

protected:
    Game & game;

};

#endif
