#ifndef _NETWORK_PLAYER_H_
#define _NETWORK_PLAYER_H_

#include <string>

#include "SDL_net.h"

#include "player.h"

class NetworkPlayer : public Player {

public:
    /* Server */
    NetworkPlayer(Color color, Uint16 port) :
        Player(color),
        sdlOwner(false),
        server(true)
    {
        init(INADDR_ANY, port);
    }
    /* Client */
    NetworkPlayer(Color color, std::string address, Uint16 port) :
        Player(color),
        sdlOwner(false),
        server(false)
    {
        init(address.c_str(), port);
    }
        
    ~NetworkPlayer();

    Action nextAction();
    void notify(Action & action);

private:
    bool sdlOwner;
    bool server;
    TCPsocket socket;
    TCPsocket remote; // server-only

    void init(const char* address, Uint16 port);

};

#endif
