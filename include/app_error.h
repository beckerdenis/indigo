#ifndef _APP_ERROR_H_
#define _APP_ERROR_H_

#include <string>
#include <exception>

class AppError : public std::exception {

public:
    AppError(const std::string & msg) :
        msg(msg) {}
    AppError(const std::string & msg, const char *libMsgC) :
        msg(msg), libMsg(libMsgC) {}

    const char* what() const throw() {
        return msg.c_str();
    }

    const std::string & getLibMsg() {
        return libMsg;
    }

private:
    std::string msg;
    std::string libMsg;

};

#endif
