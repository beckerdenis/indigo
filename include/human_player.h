#ifndef _HUMAN_PLAYER_H_
#define _HUMAN_PLAYER_H_

#include "player.h"

class Interface;

class HumanPlayer : public Player {

public:
    HumanPlayer(Color color, Interface * interface);
    ~HumanPlayer();

    Action nextAction();
    void notify(Action & action) { (void) action; } // human players share the same goban

private:
    Interface * interface;

};

#endif
