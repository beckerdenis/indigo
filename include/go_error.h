#ifndef _GO_ERROR_H_
#define _GO_ERROR_H_

#include <string>
#include <exception>

class GoError : public std::exception {

public:
    GoError() :
        msg("Generic exception") {}
    GoError(const std::string & msg) :
        msg(msg) {}

    const char* what() const throw() {
        return msg.c_str();
    }

private:
    std::string msg;

};

#endif
