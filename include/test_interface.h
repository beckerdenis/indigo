#ifndef _TEST_INTERFACE_H_
#define _TEST_INTERFACE_H_

#include <string>
#include <fstream>
#include "interface.h"

class TestInterface : public Interface {

public:
    TestInterface(Game & game, const std::string & fileName);
    ~TestInterface();

    void start();
    void update(Action & action, Player & player);

    Action nextAction(Player & player);

private:
    std::ifstream input;

};

#endif
