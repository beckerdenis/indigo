#ifndef _COMMAND_LINE_INTERFACE_H_
#define _COMMAND_LINE_INTERFACE_H_

#include "interface.h"
#include "text_game_writer.h"

class CommandLineInterface : public Interface {

public:
    CommandLineInterface(Game & game);

    void start();
    void update(Action & action, Player & player);

    Action nextAction(Player & player);

private:
    TextGameWriter writer;

};

#endif
