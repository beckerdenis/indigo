#ifndef _TEXT_GAME_READER_H_
#define _TEXT_GAME_READER_H_

#include <fstream>
#include "game_reader.h"

class Game;

class TextGameReader : public GameReader {

public:
    TextGameReader() : GameReader() {}
    TextGameReader(const std::string & filename) : GameReader(filename) {}
    void read(Game & game);
    void read_file(Game & game, std::istream & is);

};

#endif
