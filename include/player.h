#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "action.h"

class Player {

public:
    enum class Color {
        BLACK,
        WHITE
    };

    Player(Color color) : color(color) {}
    virtual ~Player() {}

    /*
     * Asks the next action from a player.
     */
    virtual Action nextAction() = 0;

    /*
     * Notifies this player of the opponent's action.
     */
    virtual void notify(Action & action) = 0;

    Color getColor() const { return color; }

    bool isBlack() const { return color == Color::BLACK; }
    bool isWhite() const { return color == Color::WHITE; }

protected:
    Color color;

};

#endif
