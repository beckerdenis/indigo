#ifndef _SDL_INTERFACE_H_
#define _SDL_INTERFACE_H_

#include <string>

#include "SDL.h"

#include "interface.h"

class Player;
class Game;

class SDLInterface : public Interface {

public:
    SDLInterface(Game & game);
    ~SDLInterface();

    void start();
    void update(Action & action, Player & player);
    
    Action nextAction(Player & player);

private:
    SDL_Window * window;
    SDL_Renderer * renderer;
    SDL_Event event;
    SDL_Texture * blackStone;
    SDL_Texture * whiteStone;
    SDL_Texture * ghostBlackStone;
    SDL_Texture * ghostWhiteStone;
    SDL_Texture * background;

    /*
     * To store ghost moves (i.e. when the mouse hovers a position,
     * a half-transparent stone appears on it).
     */
    bool ghost_move_stored;
    unsigned int ghost_x;
    unsigned int ghost_y;

    static const size_t gobanCaseSize = 32;
    /*
     * To ensure proper window refresh, a "ghost move" will be output
     * if nothing happened during the following period.
     */
    static const unsigned int period_ms = 20;

    SDL_Texture * loadTexture(const std::string & imgName);
    void internal_update();
    void event_check(Action & action);

    void draw_hoshi(unsigned int x, unsigned int y);

};

#endif
