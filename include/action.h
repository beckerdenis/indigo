#ifndef _ACTION_H_
#define _ACTION_H_

class Action {

public:
    unsigned int getX() { return x; }
    unsigned int getY() { return y; }
    bool isGoMove() { return go_move; }
    bool isGhostMove() { return ghost_move; }
    bool isPass() { return pass; }
    bool isQuit() { return quit; }
    bool isUndo() { return undo; }
    bool isNothing() { return !(go_move || undo || pass || quit || ghost_move); }

    void reset() { go_move = false; undo = false; pass = false; quit = false; ghost_move = false; }
    void setUndo() { go_move = true; undo = true; }
    void setPass() { go_move = true; pass = true; }
    void setQuit() { quit = true; }
    void setMove(unsigned int nx, unsigned int ny) { go_move = true; x = nx; y = ny; }
    void setGhostMove(unsigned int nx, unsigned int ny) { ghost_move = true; x = nx; y = ny; }

private:
    unsigned int x;
    unsigned int y;
    bool pass = false;
    bool quit = false;
    bool undo = false;
    bool go_move = false;
    bool ghost_move = false; // for graphical interface, intersection under mouse (not clicked)

};

#endif
