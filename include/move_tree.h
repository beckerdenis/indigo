#ifndef _MOVE_TREE_H_
#define _MOVE_TREE_H_

#include <vector>
#include <string>
#include "action.h"
#include "goban.h"

class MoveTree {

public:
    void addMove(Action & action, Goban & goban);
    bool isRoot() const;
    std::string goBack();
    std::string peek();

private:
    std::vector<Action> actions;
    std::vector<std::string> goban_states;

};

#endif
