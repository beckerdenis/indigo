#ifndef _INTERFACE_H_
#define _INTERFACE_H_

#include "game.h"
#include "action.h"

class Player;

/*
 * Interface for the game (graphical, command line, etc).
 */

class Interface {

public:
    Interface(Game & game) : game(game) {}
    virtual ~Interface() {}

    virtual void start() = 0;
    virtual void update(Action & action, Player & player) = 0;

    virtual Action nextAction(Player & player) = 0;

protected:
    Game & game;

};

#endif
