#ifndef _FILE_ERROR_H_
#define _FILE_ERROR_H_

#include <string>
#include <exception>

class FileError : public std::exception {

public:
    FileError(const std::string & msg) :
        msg(msg) {}
    FileError(const std::string & msg, const char *libMsgC) :
        msg(msg), libMsg(libMsgC) {}

    const char* what() const throw() {
        return msg.c_str();
    }

    const std::string & getLibMsg() {
        return libMsg;
    }

private:
    std::string msg;
    std::string libMsg;

};

#endif

