#ifndef _RANDOM_AI_PLAYER_H_
#define _RANDOM_AI_PLAYER_H_

#include "ai_player.h"

class RandomAIPlayer : public AIPlayer {

public:
    RandomAIPlayer(Color color, Game & game);
    ~RandomAIPlayer();

    Action nextAction();
    void notify(Action & action) { (void) action; }

};

#endif
