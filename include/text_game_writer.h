#ifndef _TEXT_GAME_WRITER_H_
#define _TEXT_GAME_WRITER_H_

#include <fstream>
#include "game_writer.h"

class TextGameWriter : public GameWriter {

public:
    TextGameWriter() : GameWriter() {}
    TextGameWriter(const std::string & filename) : GameWriter(filename) {}
    void write(Game & game);

private:
    void internal_write(Game & game, std::ostream & os);

};

#endif

