#ifndef _SGF_GAME_READER_H_
#define _SGF_GAME_READER_H_

#include "game_reader.h"

class Game;

class SGFGameReader : public GameReader {

public:
    SGFGameReader(const std::string & filename);
    void read(Game & goban);

};

#endif
