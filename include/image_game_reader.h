#ifndef _IMAGE_GAME_READER_H_
#define _IMAGE_GAME_READER_H_

#include <tuple>
#include <vector>

#include "player.h"
#include "game_reader.h"

/* this is to avoid including "CImg.h" here, because it's _very_ heavy */
namespace cimg_library {
    template<typename T>
    struct CImg;
}

class Game;

class ImageGameReader : public GameReader {

public:
    ImageGameReader(const std::string & filename);
    ~ImageGameReader();
    void read(Game & game);

private:
    struct pos_value_t {
        uint8_t value;
        unsigned int index;
    };

    void preProcessImage(unsigned int goban_width, unsigned int goban_height);

    std::tuple<uint8_t, uint8_t, uint8_t> getPixel(unsigned int x, unsigned int y);
    void setPixel(unsigned int x, unsigned int y, uint8_t r, uint8_t g, uint8_t b);

    cimg_library::CImg<uint8_t> & image;
    std::vector<pos_value_t> median_values;

};

#endif
