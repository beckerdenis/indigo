#ifndef _RULES_H_
#define _RULES_H_

#include <string>

class Goban;
class Game;

class Rule {

public:
    virtual ~Rule() {}

    /*
     * Return true if the rule is not violated.
     * The goban in argument is an hypothetic goban, in which the stone
     * at position (x, y) is the one to be evaluated. It can be modified
     * by this function, if needed.
     *
     * A rule must have access to everything.
     */
    virtual bool observe(Game & game, Goban & futureGoban, unsigned int x, unsigned int y) = 0;
    
    virtual std::string getErrorMsg() {
        return errorMsg;
    }

protected:
    std::string errorMsg;

};

/* Placing the stone captures. */
class CaptureRule : public Rule {

public:
    bool observe(Game & game, Goban & futureGoban, unsigned int x, unsigned int y);

};

/* Placing a stone if no liberties is disallowed. */
class NoSuicideRule : public Rule {

public:
    bool observe(Game & game, Goban & futureGoban, unsigned int x, unsigned int y);

};

/* Placing a stone, resulting in the same position than just before, is disallowed. */
class SimpleKoRule : public Rule {

public:
    bool observe(Game & game, Goban & futureGoban, unsigned int x, unsigned int y);

};

#endif
