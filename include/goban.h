#ifndef _GOBAN_H_
#define _GOBAN_H_

#include <vector>
#include <queue>
#include <array>

#include "player.h"
#include "goban_intersection.h"
#include "text_game_reader.h"
#include "sgf_game_reader.h"
#include "image_game_reader.h"

class Goban {

    friend class Game;
    friend class TextGameReader;
    friend class SGFGameReader;
    friend class ImageGameReader;

public:
    static const size_t MAX_WIDTH = 25;
    static const size_t MAX_HEIGHT = 25;

    Goban();

    void clear();

    void setSize(size_t new_width, size_t new_height) {
        width = new_width;
        height = new_height;
        clear();
    }
    size_t getWidth() const { return width; }
    size_t getHeight() const { return height; }

    /*
     * Return a string representation of the goban.
     * Each representation matches a unique positionning
     * of black and white stones.
     */
    std::string toString() const;
    void fromString(const std::string & value);
    
    const GobanIntersection & getIntersection(unsigned int x, unsigned int y) const;
    unsigned int getLiberties(unsigned int x, unsigned int y) const;
    void removeGroup(unsigned int x, unsigned int y);

private:
    size_t width;
    size_t height;
    std::array<GobanIntersection, MAX_WIDTH * MAX_HEIGHT> intersections;
    mutable std::array<bool, MAX_WIDTH * MAX_HEIGHT> visited;

    GobanIntersection & at(unsigned int x, unsigned int y) {
        return const_cast<GobanIntersection&>(getIntersection(x, y));
    }

    void putNeighbors(std::queue<unsigned int> & todo, unsigned int index) const;

};

#endif
