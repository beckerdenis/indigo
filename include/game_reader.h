#ifndef _GAME_READER_H_
#define _GAME_READER_H_

#include <string>

class Game;

/* 
 * Abstract class to read game files.
 * Implementations differ depending on file type (image, text).
 */

class GameReader {

public:
    GameReader() : no_filename(true) {}
    GameReader(const std::string & filename) : no_filename(false), filename(filename) {}
    virtual ~GameReader() {}

    virtual void read(Game & game) = 0;

protected:
    bool no_filename;
    std::string filename;

};

#endif
