#ifndef _GAME_H_
#define _GAME_H_

#include <vector>
#include <string>

#include "move_tree.h"
#include "goban.h"

class Action;
class Player;
class Rule;

class Game {

    friend class SimpleKoRule;
    friend class TextGameReader;
    friend class TextGameWriter;
    friend class SGFGameReader;
    friend class ImageGameReader;
    friend class TestInterface;
    friend class SDLInterface;

public:
    Game(std::vector<Rule*> rules);

    bool isOver() const { return gameOver; }
    Player & getCurrentPlayer() const { return *currentPlayer; }
    size_t getGobanWidth() const { return goban.getWidth(); }
    size_t getGobanHeight() const { return goban.getHeight(); }

    void execute(Action & action);
    void start(Player * black, Player * white);
    void setGobanSize(size_t width, size_t height);

private:
    MoveTree moveTree;
    Goban goban;
    Goban testGoban;

    Player * currentPlayer;
    Player * otherPlayer;

    bool lastPlayerPassed;
    bool gameOver;
    std::vector<Rule*> rules;

    void try_play(Action & action);
    void undo();

};

#endif
