#ifndef _CONFIGURATION_H_
#define _CONFIGURATION_H_

/*
 * Encapsulates the parameters for a new game.
 */

#include <string>

class Configuration {

public:
    Configuration();

    void setRules(char * rules);
    void setInitialFileName(char * fileName);
    void setInitialFromStdin(bool value);
    void setCli(bool value);
    void setTestModeWithFile(char * fileName);
    void setServerMode();
    void setClientMode(char * ip);
    void setPortNumber(int value);
    void setAI(char * name);
    
    const std::string & getRules() { return rules; }
    bool isInitialFromStdin() { return initialFromStdin; }
    const std::string & getInitialFileName() { return initialFileName; }
    bool isInitialFileSpecified() { return initialFileSpecified; }
    void setGobanSize(size_t width, size_t height);
    size_t getGobanWidth() { return gbwidth; }
    size_t getGobanHeight() { return gbheight; }
    bool isCli() { return cli; }
    bool isTestMode() { return testMode; }
    bool isServerMode() { return serverMode; }
    bool isClientMode() { return clientMode; }
    bool isVersusAI() { return versusAI; }
    std::string & getTargetIP() { return targetIP; }
    uint16_t getPortNumber() { return portNumber; }
    std::string & getNameAI() { return nameAI; }
    
private:
    std::string rules;
    std::string initialFileName;
    bool initialFileSpecified;
    bool initialFromStdin;
    bool testMode;
    bool cli; // command line interface
    size_t gbwidth;
    size_t gbheight;
    bool serverMode;
    bool clientMode;
    std::string targetIP;
    uint16_t portNumber;
    std::string nameAI;
    bool versusAI;

};

#endif
