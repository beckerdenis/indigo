#ifndef _GOBAN_INTERSECTION_H_
#define _GOBAN_INTERSECTION_H_

#include "player.h"

class GobanIntersection {

public:
    GobanIntersection() : empty(true) {}
    bool isEmpty() const { return empty; }
    void reset() { empty = true; }
    
    void putStone(Player::Color new_owner);
    Player::Color getOwner() const;

private:
    bool empty;
    Player::Color owner;

};

#endif
