#ifndef _GAME_WRITER_H_
#define _GAME_WRITER_H_

#include <string>

class Game;

class GameWriter {

public:
    GameWriter() : no_filename(true) {}
    GameWriter(const std::string & filename) : no_filename(false), filename(filename) {}
    virtual ~GameWriter() {}

    virtual void write(Game & game) = 0;

protected:
    bool no_filename;
    std::string filename;

};

#endif
