#include "goban_intersection.h"
#include "app_error.h"

void GobanIntersection::putStone(Player::Color new_owner)
{
    empty = false;
    owner = new_owner;
}

Player::Color GobanIntersection::getOwner() const
{
    if (empty) {
        throw AppError("Call to 'getOwner' on empty intersection.");
    }
    return owner;
}
