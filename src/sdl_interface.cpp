#include "SDL.h"
#include "SDL_image.h"
#include "SDL2_gfxPrimitives.h"

#include "sdl_interface.h"
#include "player.h"
#include "game.h"
#include "app_error.h"
#include "action.h"

SDLInterface::SDLInterface(Game & game) :
    Interface(game),
    window(nullptr),
    renderer(nullptr),
    blackStone(nullptr),
    whiteStone(nullptr),
    ghostBlackStone(nullptr),
    ghostWhiteStone(nullptr),
    background(nullptr),
    ghost_move_stored(false)
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        SDL_Quit();
        throw AppError("Impossible to start SDL, switching to CLI.", SDL_GetError());
    }
    window = SDL_CreateWindow("Indigo",
            SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
            gobanCaseSize * game.getGobanWidth(), gobanCaseSize * game.getGobanHeight(),
            SDL_WINDOW_HIDDEN);
    if (window == nullptr) {
        SDL_Quit();
        throw AppError("Impossible to create SDL window, switching to CLI.", SDL_GetError());
    }
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == nullptr) {
        SDL_DestroyWindow(window);
        SDL_Quit();
        throw AppError("Impossible to create SDL renderer, switching to CLI.", SDL_GetError());
    }
    if (!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG)) {
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
        throw AppError("Impossible to create SDL image, switching to CLI.", SDL_GetError());
    }
    try {
        blackStone = loadTexture("black.png");
        whiteStone = loadTexture("white.png");

        ghostBlackStone = loadTexture("black.png");
        SDL_SetTextureAlphaMod(ghostBlackStone, 128);
        ghostWhiteStone = loadTexture("white.png");
        SDL_SetTextureAlphaMod(ghostWhiteStone, 128);
        
        background = loadTexture("background.png");
    } catch (AppError & e) {
        IMG_Quit();
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
        throw e;
    }
    internal_update();
    SDL_RenderPresent(renderer);
}

SDLInterface::~SDLInterface()
{
    IMG_Quit();
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void SDLInterface::start()
{
    SDL_ShowWindow(window);
    internal_update();
    SDL_RenderPresent(renderer);
}

void SDLInterface::update(Action & action, Player & player)
{
    SDL_Rect rect;
    
    internal_update();

    if (action.isGhostMove()) {
        if (!game.goban.getIntersection(action.getX(), action.getY()).isEmpty()) {
            ghost_move_stored = false;
        } else {
            ghost_move_stored = true;
            ghost_x = action.getX();
            ghost_y = action.getY();
        }
    } else if (!action.isNothing()) {
        ghost_move_stored = false;
    }

    // ghost move
    if (ghost_move_stored) {
        SDL_QueryTexture(blackStone, nullptr, nullptr, &rect.w, &rect.h); // just to get the stone image size
        rect.x = ghost_x * gobanCaseSize + (gobanCaseSize - rect.w) / 2;
        rect.y = ghost_y * gobanCaseSize + (gobanCaseSize - rect.h) / 2;
        SDL_RenderCopy(renderer, player.isBlack() ? ghostBlackStone : ghostWhiteStone, nullptr, &rect);
    }
    
    SDL_RenderPresent(renderer);
}

void SDLInterface::draw_hoshi(unsigned int x, unsigned int y)
{
    unsigned int radius_x = 2;
    unsigned int radius_y = radius_x + 1;
    filledEllipseRGBA(renderer, x * gobanCaseSize + (gobanCaseSize / 2),
            y * gobanCaseSize + (gobanCaseSize / 2), radius_x, radius_y, 51, 37, 7, 255);
}

void SDLInterface::internal_update()
{
    SDL_Rect rect;
    // draw background
    SDL_QueryTexture(background, nullptr, nullptr, &rect.w, &rect.h);
    for (rect.x = 0; unsigned(rect.x) < gobanCaseSize * game.getGobanWidth(); rect.x += rect.w) {
        for (rect.y = 0; unsigned(rect.y) < gobanCaseSize * game.getGobanHeight(); rect.y += rect.h) {
            SDL_RenderCopy(renderer, background, nullptr, &rect);
        }
    }
    // draw lines
    SDL_SetRenderDrawColor(renderer, 51, 37, 7, 255);
    for (unsigned int x = 0; x < game.getGobanWidth(); x++) {
        SDL_RenderDrawLine(renderer,
                x * gobanCaseSize + gobanCaseSize / 2,
                gobanCaseSize / 2,
                x * gobanCaseSize + gobanCaseSize / 2,
                game.getGobanHeight() * gobanCaseSize - gobanCaseSize / 2);
    }
    for (unsigned int y = 0; y < game.getGobanHeight(); y++) {
        SDL_RenderDrawLine(renderer,
                gobanCaseSize / 2,
                y * gobanCaseSize + gobanCaseSize / 2,
                game.getGobanWidth() * gobanCaseSize - gobanCaseSize / 2,
                y * gobanCaseSize + gobanCaseSize / 2);
    }
    // draw hoshis
    unsigned int hoshi_x = 4;
    unsigned int hoshi_y = 4;
    if (game.getGobanWidth() <= 9) {
        hoshi_x = 3;
    }
    if (game.getGobanHeight() <= 9) {
        hoshi_y = 3;
    }
    if (game.getGobanWidth() >= 7 || game.getGobanHeight() >= 7) {
        draw_hoshi(hoshi_x - 1, hoshi_y - 1);
        draw_hoshi(hoshi_x - 1, game.getGobanHeight() - hoshi_y);
        draw_hoshi(game.getGobanWidth() - hoshi_x, hoshi_y - 1);
        draw_hoshi(game.getGobanWidth() - hoshi_x, game.getGobanHeight() - hoshi_y);
    }
    if (game.getGobanWidth() >= 17 && game.getGobanWidth() % 2 == 1) {
        draw_hoshi(game.getGobanWidth() / 2, hoshi_y - 1);
        draw_hoshi(game.getGobanWidth() / 2, game.getGobanHeight() - hoshi_y);
    }
    if (game.getGobanHeight() >= 17 && game.getGobanHeight() % 2 == 1) {
        draw_hoshi(hoshi_x - 1, game.getGobanHeight() / 2);
        draw_hoshi(game.getGobanWidth() - hoshi_x, game.getGobanHeight() / 2);
    }
    if (game.getGobanWidth() % 2 == 1 && game.getGobanHeight() % 2 == 1 && (game.getGobanWidth() > 7 || game.getGobanHeight() > 7)) {
        draw_hoshi(game.getGobanWidth() / 2, game.getGobanHeight() / 2);
    }
    // draw stones
    SDL_QueryTexture(blackStone, nullptr, nullptr, &rect.w, &rect.h);
    for (unsigned int x = 0; x < game.getGobanWidth(); x++) {
        for (unsigned int y = 0; y < game.getGobanHeight(); y++) {
            if (!game.goban.getIntersection(x, y).isEmpty()) {
                rect.x = x * gobanCaseSize + (gobanCaseSize - rect.w) / 2;
                rect.y = y * gobanCaseSize + (gobanCaseSize - rect.h) / 2;
                if (game.goban.getIntersection(x, y).getOwner() == Player::Color::BLACK) {
                    SDL_RenderCopy(renderer, blackStone, nullptr, &rect);
                } else {
                    SDL_RenderCopy(renderer, whiteStone, nullptr, &rect);
                }
            }
        }
    }
}

void SDLInterface::event_check(Action & action)
{
    if (!SDL_PollEvent(&event)) {
        return;
    } else if (event.type == SDL_WINDOWEVENT) {
        if (event.window.event == SDL_WINDOWEVENT_LEAVE) {
            action.reset();
            ghost_move_stored = false;
        }
    } else if (event.type == SDL_QUIT) {
        action.setQuit();
    } else if (event.type == SDL_MOUSEMOTION) {
        unsigned int x = event.motion.x / gobanCaseSize;
        unsigned int y = event.motion.y / gobanCaseSize;
        action.setGhostMove(x, y);
    } else if (event.button.type == SDL_MOUSEBUTTONUP) {
        if (event.button.button == SDL_BUTTON_LEFT) {
            unsigned int x = event.button.x / gobanCaseSize;
            unsigned int y = event.button.y / gobanCaseSize;
            action.setMove(x, y);
        } else if (event.button.button == SDL_BUTTON_RIGHT) {
            action.setPass();
        } else if (event.button.button == SDL_BUTTON_MIDDLE) {
            action.setUndo();
        }
    }
}

Action SDLInterface::nextAction(Player & player)
{
    (void) player;
    Action action;

    unsigned int initial_time = SDL_GetTicks();

    while (true) {
        event_check(action);
        if (!action.isNothing()) {
            return action;
        }

        // temporisation checking: if nothing happened during the period, we return
        // an empty action (to still refresh the window)
        if (SDL_GetTicks() - initial_time > period_ms) {
            action.reset();
            return action;
        }
    }
}

SDL_Texture * SDLInterface::loadTexture(const std::string & imgName)
{
    SDL_Surface * surface = IMG_Load(("img/" + imgName).c_str());
    if (surface == nullptr) {
        throw AppError("Impossible to load an image, switching to CLI.", IMG_GetError());
    }
    SDL_Texture * texture = SDL_CreateTextureFromSurface(renderer, surface);
    if (texture == nullptr) {
        throw AppError("Impossible to convert surface to texture, switching to CLI.", IMG_GetError());
    }
    SDL_FreeSurface(surface);
    return texture;
}
