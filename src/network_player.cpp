#include "SDL.h"

#include "network_player.h"
#include "app_error.h"

void NetworkPlayer::init(const char * address, Uint16 port)
{
    if (!SDL_WasInit(SDL_INIT_VIDEO)) {
        if (SDL_Init(0) < 0) {
            SDL_Quit();
            throw AppError("Impossible to start SDL for networking.", SDL_GetError());
        }
        sdlOwner = true;
    }
    if (SDLNet_Init() < 0) {
        if (sdlOwner) {
            SDL_Quit();
        }
        throw AppError("Impossible to start SDLNet.", SDLNet_GetError());
    }
    IPaddress ip;
    if (SDLNet_ResolveHost(&ip, address, port) < 0) {
        SDLNet_Quit();
        if (sdlOwner) {
            SDL_Quit();
        }
        throw AppError("Impossible to resolve IP address/hostname.", SDLNet_GetError());
    }
    socket = SDLNet_TCP_Open(&ip);
    if (!socket) {
        SDLNet_Quit();
        if (sdlOwner) {
            SDL_Quit();
        }
        throw AppError("Impossible to open network connection.", SDLNet_GetError());
    }
    // server
    if (server) {
        SDL_Event e;
        int count = 0;
        while (count++ < 100 && !(remote = SDLNet_TCP_Accept(socket))) {
            SDL_Delay(200);
            SDL_PollEvent(&e);
            if (e.type == SDL_QUIT) {
                break;
            }
        }
        if (!remote) {
            SDLNet_TCP_Close(socket);
            SDLNet_Quit();
            if (sdlOwner) {
                SDL_Quit();
            }
            throw AppError("No client has been connected.", SDLNet_GetError());
        }
    }
}

NetworkPlayer::~NetworkPlayer()
{
    if (server) {
        SDLNet_TCP_Close(remote);
    }
    SDLNet_TCP_Close(socket);
    SDLNet_Quit();
    if (sdlOwner) {
        SDL_Quit();
    }
}

Action NetworkPlayer::nextAction()
{
    Action action;
    // bits of data
    // [0..6]   x coordinate
    // [7]      1 = pass, 0 = normal
    // [8..14]  y coordinate
    // [15]     1 = undo, 0 = normal
    /*uint8_t data[2];
    if (SDLNet_TCP_Recv(server ? remote : socket, data, sizeof(data)) <= 0) {
        throw AppError("Error with the network connection.\nThe other player probably exited. Game over.", SDLNet_GetError());
    }
    coup.x = static_cast<int>(data[0] & UINT8_C(0x7f));
    coup.y = static_cast<int>(data[1] & UINT8_C(0x7f));
    coup.pass = static_cast<bool>(data[0] & UINT8_C(0x80));
    coup.undo = static_cast<bool>(data[1] & UINT8_C(0x80));
    return true;
    */
    return action;
}

void NetworkPlayer::notify(Action & action)
{
    /*
    uint8_t data[2];
    if (coup.x > 0x7f || coup.y > 0x7f) {
        throw AppError("Coup unexpectedly out of bounds. Game over.", SDLNet_GetError());
    }
    data[0] = (coup.pass ? 0x80 : 0) | static_cast<uint8_t>(coup.x);
    data[1] = (coup.undo ? 0x80 : 0) | static_cast<uint8_t>(coup.y);
    if (SDLNet_TCP_Send(server ? remote : socket, data, sizeof(data)) < static_cast<int>(sizeof(data))) {
        throw AppError("Sending coup is impossible, the target isn't reachable. Game over.", SDLNet_GetError());
    }*/
    (void) action;
}
