#include "rules.h"
#include "game.h"
#include "goban.h"

bool CaptureRule::observe(Game & game, Goban & testGoban, unsigned int x, unsigned int y)
{
    (void) game;
    int xdiff[]{-1,  1,  0,  0};
    int ydiff[]{ 0,  0, -1,  1};

    for (int i = 0; i < 4; ++i) {
        unsigned int tx = (unsigned int) (xdiff[i] + (int) x);
        unsigned int ty = (unsigned int) (ydiff[i] + (int) y);
        if (tx < testGoban.getWidth() && ty < testGoban.getHeight()) {
            if (!testGoban.getIntersection(tx, ty).isEmpty()) {
                if (testGoban.getLiberties(tx, ty) == 0) {
                    testGoban.removeGroup(tx, ty);
                }
            }
        }
    }
    return true;
}

bool NoSuicideRule::observe(Game & game, Goban & testGoban, unsigned int x, unsigned int y)
{
    (void) game;
    return testGoban.getLiberties(x, y) > 0;
}

bool SimpleKoRule::observe(Game & game, Goban & testGoban, unsigned int x, unsigned int y)
{
    (void) x;
    (void) y;

    if (game.moveTree.isRoot()) {
        return true;
    }

    return testGoban.toString() != game.moveTree.peek();
}
