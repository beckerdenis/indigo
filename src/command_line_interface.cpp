#include <cstdlib>

#include <iostream>
#include <fstream>
#include <sstream>

#include "player.h"
#include "game.h"
#include "command_line_interface.h"

#define CMD_IS(cmd) (line.compare(cmd) == 0)

CommandLineInterface::CommandLineInterface(Game & game) :
    Interface(game),
    writer() // write on std::std::cout
{
}

void CommandLineInterface::start()
{
}

void CommandLineInterface::update(Action & action, Player & player)
{
    (void) action;
    (void) player;
}

Action CommandLineInterface::nextAction(Player & player)
{
    writer.write(game);

    Action action;
    const char* name = player.isBlack() ? "black" : "white";
    std::cout << "Your turn: " << name << std::endl;
    while (true) {
        std::cout << "Coup ? (type 'help' to have some) " << std::endl;
        std::string line;
        getline(std::cin, line);
        if (line.empty()) {
            action.setPass();
            return action;
        } else if (CMD_IS("help")) {
            std::cout << "Available commands:" << std::endl;
            std::cout << "  X Y        Play at position (X, Y) where X and Y are" << std::endl;
            std::cout << "             coordinates, top-left is (1, 1), bottom-left is (1, 19)" << std::endl;
            std::cout << "  undo       Undo the last move" << std::endl;
            std::cout << "  help       Well, this is it" << std::endl;
            std::cout << "  exit       Exit the game" << std::endl;
        } else if (CMD_IS("undo")) {
            action.setUndo();
            return action;
        } else if (CMD_IS("exit")) {
            action.setQuit();
            return action;
        } else {
            std::stringstream str(line);
            str.exceptions(std::ifstream::failbit | std::ifstream::badbit);
            try {
                unsigned int x, y;
                str >> x >> y;
                action.setMove(x - 1, y - 1);
                return action;
            } catch (std::ifstream::failure e) {
                std::cout << "Invalid command, maybe try 'help'." << std::endl;
            }
        }
    }
}
