#include "game.h"
#include "goban.h"
#include "player.h"
#include "action.h"
#include "go_error.h"
#include "rules.h"

Game::Game(std::vector<Rule*> rules) :
    lastPlayerPassed(false),
    gameOver(false),
    rules(rules)
{
}

void Game::setGobanSize(size_t width, size_t height)
{
    goban.setSize(width, height);
    testGoban.setSize(width, height);
}

void Game::start(Player * black, Player * white)
{
    currentPlayer = black;
    otherPlayer = white;
}

void Game::execute(Action & action)
{
    if (action.isQuit()) {
        gameOver = true;
    } else if (action.isPass() && lastPlayerPassed) {
        gameOver = true;
    } else if (action.isGoMove()) {
        
        bool valid = true;
        if (action.isUndo()) {
            undo();
        } else if (action.isPass()) {
            moveTree.addMove(action, goban);
        } else {
            try_play(action);
        }
        
        if (valid) {
            otherPlayer->notify(action);
            std::swap(currentPlayer, otherPlayer);
        }
    }
    lastPlayerPassed = action.isPass();
}

void Game::try_play(Action & action)
{
    // check range and empty
    if (!goban.getIntersection(action.getX(), action.getY()).isEmpty()) {
        throw GoError("Intersection not empty.");
    }

    testGoban.fromString(goban.toString());
    testGoban.at(action.getX(), action.getY()).putStone(currentPlayer->getColor());

    for (auto & rule : rules) {
        if (!rule->observe(*this, testGoban, action.getX(), action.getY())) {
            throw GoError(rule->getErrorMsg());
        }
    }

    moveTree.addMove(action, goban);
    goban.fromString(testGoban.toString());
}

void Game::undo()
{
    if (moveTree.isRoot()) {
        throw GoError();
    }

    goban.fromString(moveTree.goBack());
}
