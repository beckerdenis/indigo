#include <iostream>
#include <fstream>

#include "text_game_reader.h"
#include "player.h"
#include "goban.h"
#include "game.h"

void TextGameReader::read(Game & game)
{
    if (no_filename) {
        read_file(game, std::cin);
        std::string line;
        getline(std::cin, line);
    } else {
        std::ifstream file(filename);
        read_file(game, file);
    }
}

void TextGameReader::read_file(Game & game, std::istream & is)
{
    game.goban.clear();
    
    size_t w, h;
    is >> w >> h;

    game.setGobanSize(w, h);
    for (unsigned int y = 0; y < h; y++) {
        for (unsigned int x = 0; x < w; x++) {
            char c;
            is >> c;

            if (c == 'B' || c == 'b') {
                game.goban.at(x, y).putStone(Player::Color::BLACK);
            } else if (c == 'W' || c == 'w') {
                game.goban.at(x, y).putStone(Player::Color::WHITE);
            }
        }
    }
}
