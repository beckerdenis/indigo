#include <sstream>
#include <queue>

#include "goban.h"
#include "goban_intersection.h"
#include "player.h"
#include "go_error.h"
#include "app_error.h"
#include "action.h"

Goban::Goban() :
    width(MAX_WIDTH),
    height(MAX_HEIGHT)
{
}

const GobanIntersection & Goban::getIntersection(unsigned int x, unsigned int y) const
{
    if (x < width && y < height) {
        return intersections[y * MAX_WIDTH + x];
    }
    throw GoError("Out of goban (" + std::to_string(x) + ", " + std::to_string(y) + ").");
}

void Goban::putNeighbors(std::queue<unsigned int> & todo, unsigned int index) const
{
    unsigned int x = index % MAX_WIDTH;
    unsigned int y = index / MAX_WIDTH;
    if (x > 0) {
        index = y * MAX_WIDTH + x - 1;
        if (!visited[index]) {
            todo.push(index);
        }
    }
    if (x < width - 1) {
        index = y * MAX_WIDTH + x + 1;
        if (!visited[index]) {
            todo.push(index);
        }
    }
    if (y > 0) {
        index = (y - 1) * MAX_WIDTH + x;
        if (!visited[index]) {
            todo.push(index);
        }
    }
    if (y < height - 1) {
        index = (y + 1) * MAX_WIDTH + x;
        if (!visited[index]) {
            todo.push(index);
        }
    }
}

void Goban::removeGroup(unsigned int x, unsigned int y)
{
    unsigned int index = y * MAX_WIDTH + x;

    visited.fill(false);
    visited[index] = true;

    std::queue<unsigned int> todo;
    putNeighbors(todo, index);

    Player::Color color = intersections[index].getOwner();
    intersections[index].reset();
    
    // invariant: all indexes in "todo" are indexes of stones
    // around a stone from the group to remove
    while (!todo.empty()) {
        index = todo.front();
        todo.pop();

        if (!intersections[index].isEmpty()) {
            if (intersections[index].getOwner() == color) {
                visited[index] = true;
                intersections[index].reset();
                putNeighbors(todo, index);
            }
        }
    }
}

unsigned int Goban::getLiberties(unsigned int x, unsigned int y) const
{
    unsigned int liberties = 0;
    unsigned int index = y * MAX_WIDTH + x;

    visited.fill(false);
    visited[index] = true;
    
    std::queue<unsigned int> todo;
    putNeighbors(todo, index);

    Player::Color color = intersections[index].getOwner();
    
    // invariant: all indexes in "todo" are indexes of intersections
    // around a stone from the group to check
    while (!todo.empty()) {
        index = todo.front();
        todo.pop();

        if (intersections[index].isEmpty()) {
            if (!visited[index]) {
                visited[index] = true;
                ++liberties;
            }
        } else if (intersections[index].getOwner() == color) {
            visited[index] = true;
            putNeighbors(todo, index);
        }
    }

    return liberties;
}

void Goban::clear()
{
    for (auto & gc : intersections) {
        gc.reset();
    }
}

std::string Goban::toString() const
{
    std::stringstream stream;
    for (unsigned int y = 0; y < height; y++) {
        for (unsigned int x = 0; x < width; x++) {
            if (getIntersection(x, y).isEmpty()) {
                stream << '.';
            } else if (getIntersection(x, y).getOwner() == Player::Color::BLACK) {
                stream << 'b';
            } else {
                stream << 'w';
            }
        }
    }
    return stream.str();
}

void Goban::fromString(const std::string & value)
{
    for (unsigned int y = 0; y < height; ++y) {
        for (unsigned int x = 0; x < width; ++x) {
            char c = value[y * width + x];
            if (c == '.') {
                at(x, y).reset();
            } else if (c == 'b') {
                at(x, y).putStone(Player::Color::BLACK);
            } else if (c == 'w') {
                at(x, y).putStone(Player::Color::WHITE);
            }
        }
    }
}
