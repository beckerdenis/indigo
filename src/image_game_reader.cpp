#include "CImg.h"
#include "image_game_reader.h"
#include "player.h"
#include "goban.h"
#include "game.h"
#include "app_error.h"

#include <vector>
#include <algorithm>
#include <numeric>

ImageGameReader::ImageGameReader(const std::string & filename) :
    GameReader(filename),
    image(*new cimg_library::CImg<uint8_t>(filename.c_str()))
{
}

ImageGameReader::~ImageGameReader()
{
    delete &image;
}

std::tuple<uint8_t, uint8_t, uint8_t> ImageGameReader::getPixel(unsigned int x, unsigned int y)
{
    return std::make_tuple(image(x, y, 0, 0), image(x, y, 0, 1), image(x, y, 0, 2));
}

void ImageGameReader::setPixel(unsigned int x, unsigned int y, uint8_t r, uint8_t g, uint8_t b)
{
    image(x, y, 0, 0) = r;
    image(x, y, 0, 1) = g;
    image(x, y, 0, 2) = b;
}

static uint8_t contrast_change(uint8_t orig, int contrast) // from -128 to 128
{
    int orig_int = (int) orig;
    int result = 259 * (contrast + 255) * (orig_int - 128) / (255 * (259 - contrast)) + 128;
    if (result < 0) {
        return 0;
    } else if (result > 255) {
        return 255;
    }
    return result < 0 ? 0 : (result > 255 ? 255 : result);
}

static uint8_t rgb_to_gray(uint8_t r, uint8_t g, uint8_t b)
{
    unsigned int r_int = (unsigned int) r;
    unsigned int g_int = (unsigned int) g;
    unsigned int b_int = (unsigned int) b;
    unsigned int pixel = (21 * r_int + 72 * g_int + 7 * b_int) / 100;
    return pixel > 255 ? 255 : pixel;
}

static void split_regions(unsigned int image_dim, unsigned int goban_dim, std::vector<unsigned int> & regions)
{
    unsigned int region_dim = image_dim / goban_dim;
    for (unsigned int i = 0; i < goban_dim; ++i) {
        regions.push_back(region_dim);
    }

    unsigned int diff = image_dim - region_dim * goban_dim;
    while (diff > 0) {
        ++regions[rand() % regions.size()];
        --diff;
    }
}

static uint8_t get_median(std::vector<uint8_t> & values)
{
    if (values.size() == 0) {
        throw AppError("Grayscale vector is empty...");
    }

    std::sort(values.begin(), values.end());
    if (values.size() % 2 == 0) {
        return (values[(values.size() - 1) / 2] + values[values.size() / 2]) / 2;
    }
    return values[values.size() / 2];
}

void ImageGameReader::preProcessImage(unsigned int goban_width, unsigned int goban_height)
{
    std::vector<unsigned int> regions_w;
    std::vector<unsigned int> regions_h;
    split_regions(image.width(), goban_width, regions_w);
    split_regions(image.height(), goban_height, regions_h);

    // Go through goban intersections

    unsigned int sum_y = 0;
    for (unsigned int gy = 0; gy < goban_height; gy++) {
        unsigned int sum_x = 0;
        for (unsigned int gx = 0; gx < goban_width; gx++) {

            // Go through each pixel for the current goban intersection
            
            std::vector<uint8_t> local_gray_values;

            for (unsigned int iy = 0; iy < regions_h[gy]; ++iy) {
                for (unsigned int ix = 0; ix < regions_w[gx]; ++ix) {
                    unsigned int x = sum_x + ix;
                    unsigned int y = sum_y + iy;

                    uint8_t r, g, b;
                    std::tie(r, g, b) = getPixel(x, y);
                    r = contrast_change(r, 128);
                    g = contrast_change(g, 128);
                    b = contrast_change(b, 128);
                    local_gray_values.push_back(rgb_to_gray(r, g, b));
                }
            }

            median_values.push_back({get_median(local_gray_values), gy * goban_width + gx});

            sum_x += regions_w[gx];
        }

        sum_y += regions_h[gy];
    }
}

void ImageGameReader::read(Game & game)
{
    game.goban.clear();

    size_t w = game.getGobanWidth();
    size_t h = game.getGobanHeight();

    /*
     * Fill median_values vector with grayscale median values
     * for each image region corresponding to an intersection.
     */
    preProcessImage(w, h);

    auto pos_value_comparator = [] (const pos_value_t & l, const pos_value_t & r) -> bool {
        return l.value < r.value;
    };

    /*
     * Normally, median_values vector contains 3 types of values:
     * low (black stones = B), high (white stones = W) and somewhere
     * between (no stone = O).
     * 
     * Example, for a picture with this 3x3 goban:
     *
     *   . b .
     *   . . w
     *   w b .
     *
     * We obtain the following grayscale median values:
     *
     *   190  15 193
     *   200 189 243
     *   246  18 193
     *
     * The goal is to find to which class (B, W or O) each value does belong.
     * For that, we try to find "gaps" in value changing. Normally
     * there should be 2 gaps: from black to no stone, and from
     * no stone to white.
     *
     * First step for finding these gaps is to sort the values. Example:
     *
     *   15 18 189 190 193 193 200 243 246
     *
     * Then we compute the diff vector of such values. Example:
     *
     *     3  171  1  3  0  7  43  3
     *
     * On this diff vector (of sorted values) there should be two values
     * substantially higher than the others. For the example, the two
     * values in question are 43 and 171. We take those two maximum values,
     * find to which original values they refer to (to avoid this step,
     * the original coordinates are stored) and decide that they
     * are the thresholds from one class to the other.
     *
     * If there are not 2 clear maximums, there can only be one clear maximum.
     * If there is not any one clear maximum, we can deduce that all data belong
     * to the same category... which is empty goban, because it's nonsense to
     * fill a goban with stones of the same color. Nonsense. Don't do that.
     *
     * What's a "clear" maximum? Good question.
     */
    std::vector<pos_value_t> median_copy = median_values;
    std::sort(median_copy.begin(), median_copy.end(), pos_value_comparator);

    std::vector<pos_value_t> diff_vector;
    diff_vector.push_back(median_copy.front());
    for (size_t i = 1; i < median_copy.size(); ++i) {
        diff_vector.push_back({
            static_cast<uint8_t>(median_copy[i].value - median_copy[i - 1].value), median_copy[i].index
        });
    }

    // do not include first value in the sort: it's not a diff value
    std::sort(diff_vector.begin() + 1, diff_vector.end(), pos_value_comparator);
    uint8_t max_one = diff_vector[diff_vector.size() - 1].value;
    uint8_t max_two = diff_vector[diff_vector.size() - 2].value;
    uint8_t max_three = diff_vector[diff_vector.size() - 3].value;
    bool clear_max_one = (max_one - max_three) > 15;
    bool clear_max_two = (max_two - max_three) > 15;

    if (clear_max_one) {
        unsigned int threshold_white = median_values[diff_vector[diff_vector.size() - 1].index].value;
        unsigned int threshold_black = median_values[diff_vector[diff_vector.size() - 2].index].value;
        if (threshold_white < threshold_black) {
            std::swap(threshold_white, threshold_black);
        }

        if (!clear_max_two) {
            if (threshold_white < 150) {
                threshold_black = threshold_white;
                threshold_white = 256;
            } else {
                threshold_black = 0;
            }
        }
        printf("TW: %d, TB: %d\n", threshold_white, threshold_black);

        for (unsigned int y = 0; y < h; ++y) {
            for (unsigned int x = 0; x < w; ++x) {
                if (threshold_black > (unsigned int) median_values[y * w + x].value) {
                    game.goban.at(x, y).putStone(Player::Color::BLACK);
                } else if (threshold_white <= (unsigned int) median_values[y * w + x].value) {
                    game.goban.at(x, y).putStone(Player::Color::WHITE);
                }
            }
        }
    }
}
