#include "move_tree.h"

void MoveTree::addMove(Action & action, Goban & goban)
{
    actions.push_back(action);
    goban_states.push_back(goban.toString());
}

bool MoveTree::isRoot() const
{
    return actions.empty();
}

std::string MoveTree::goBack()
{
    std::string last = goban_states.back();
    actions.pop_back();
    goban_states.pop_back();
    return last;
}

std::string MoveTree::peek()
{
    return goban_states.back();
}
