#include "human_player.h"
#include "interface.h"

HumanPlayer::HumanPlayer(Color color, Interface * interface) :
    Player(color),
    interface(interface)
{
}

HumanPlayer::~HumanPlayer()
{
}

Action HumanPlayer::nextAction()
{
    return interface->nextAction(*this);
}
