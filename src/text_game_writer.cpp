#include <iostream>
#include <fstream>

#include "text_game_writer.h"
#include "player.h"
#include "goban.h"
#include "game.h"

void TextGameWriter::write(Game & game)
{
    if (no_filename) {
        internal_write(game, std::cout);
    } else {
        std::ofstream file(filename);
        internal_write(game, file);
    }
}

void TextGameWriter::internal_write(Game & game, std::ostream & os)
{
    os << game.goban.getWidth() << ' ' << game.goban.getHeight() << std::endl;
    for (unsigned int y = 0; y < game.goban.getHeight(); y++) {
        for (unsigned int x = 0; x < game.goban.getWidth(); x++) {
            if (game.goban.getIntersection(x, y).isEmpty()) {
                os << '.';
            } else if (game.goban.getIntersection(x, y).getOwner() == Player::Color::BLACK) {
                os << 'B';
            } else {
                os << 'W';
            }
            
            if (x != game.goban.getWidth() - 1) {
                os << ' ';
            } else {
                os << std::endl;
            }
        }
    }
}

