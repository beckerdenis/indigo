#include <cstring>
#include "sgf_game_reader.h"
#include "move_tree.h"
#include "app_error.h"
#include "player.h"
#include "action.h"
#include "goban.h"
#include "game.h"

// SGFC include files (C, not C++)
extern "C" {
#include "all.h"
#include "protos.h"

#undef BLACK
#undef WHITE
}

static SGFInfo sgf;
static struct Node * node_it;

SGFGameReader::SGFGameReader(const std::string & filename) :
    GameReader(filename)
{
    memset(&sgf, 0, sizeof(struct SGFInfo));
    sgf.name = const_cast<char*>(filename.c_str());
    
    LoadSGF(&sgf);
    ParseSGF(&sgf);
    
    if (error_count) {
        FreeSGFInfo(&sgf);
        throw AppError("An error occured while parsing SGF.");
    }
    
    node_it = sgf.info->root->next;
}

bool hasNextMove()
{
    return node_it != NULL;
}

void nextMove(Action & action)
{
    struct Property * prop;

    prop = Find_Property(node_it, TKN_B);
    if (prop != NULL) {
        unsigned int x = prop->value->value[0] - 'a';
        unsigned int y = prop->value->value[1] - 'a';
        action.setMove(x, y);
        printf("Black Move: %d %d\n", x, y);
    } else {
        prop = Find_Property(node_it, TKN_W);
        if (prop != NULL) {
            unsigned int x = prop->value->value[0] - 'a';
            unsigned int y = prop->value->value[1] - 'a';
            action.setMove(x, y);
            printf("White Move: %d %d\n", x, y);
        } else {
            action.setGhostMove(0, 0);
        }
    }

    node_it = node_it->next;
}

void SGFGameReader::read(Game & game)
{
    game.goban.clear();

    game.setGobanSize(sgf.info->bwidth, sgf.info->bheight);

    Player::Color color = Player::Color::BLACK;
    while (hasNextMove()) {
        Action action;
        nextMove(action);

        game.moveTree.addMove(action, game.goban);
        game.goban.at(action.getX(), action.getY()).putStone(color);
        color = (color == Player::Color::BLACK) ? Player::Color::WHITE : Player::Color::BLACK;
    }

    FreeSGFInfo(&sgf);
}
