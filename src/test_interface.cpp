#include <cstdlib>

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <bitset>

#include "player.h"
#include "game.h"
#include "test_interface.h"
#include "text_game_reader.h"
#include "text_game_writer.h"
#include "image_game_reader.h"
#include "go_error.h"

#define CMD_IS(cmd) (line.compare(cmd) == 0)

TestInterface::TestInterface(Game & game, const std::string & fileName) :
    Interface(game),
    input(fileName)
{
    // initialization: read initial goban
    int w, h;
    input >> w >> h;
    for (int i = 0; i < w * h; i++) {
        char bin;
        input >> bin;
    }
    // move input file to the first command
    std::string nothing;
    getline(input, nothing);
}

TestInterface::~TestInterface()
{
}

void TestInterface::start()
{
}

void TestInterface::update(Action & action, Player & player)
{
    (void) action;
    (void) player;
}

Action TestInterface::nextAction(Player & player)
{
    (void) player;

    TextGameWriter writer;
    TextGameReader reader;
    Action action;
    while (true) {
        std::string line;
        getline(input, line);
        std::cout << "command:" << line << std::endl;
        if (line.empty()) {
            std::cout << "player pass" << std::endl;
            action.setPass();
            return action;
        } else if (CMD_IS("load_img")) {
            /*
             * Load a game from an image.
             * The current size will be used (mandatory to provide a goban first).
             */
            std::string image_name;
            getline(input, image_name);
            ImageGameReader image_reader(image_name);
            image_reader.read(game);
        } else if (CMD_IS("help")) {
        } else if (CMD_IS("exit")) {
            action.setQuit();
            return action;
        } else if (CMD_IS("undo")) {
            action.setUndo();
            return action;
        } else if (CMD_IS("load")) {
            /*
             * Load a game from input
             * The same game size will be used,
             * you just have to input the list of points
             * separated by spaces and/or line breaks
             * Example for a 5x5 game, you can input:
             * . . . . .
             * . . b . .
             * . . b w .
             * . b w . w
             * . . . . .
             */
            std::cout << "here is the old goban:" << std::endl;
            writer.write(game);
            reader.read_file(game, input);
            std::cout << "here is the new goban:" << std::endl;
            writer.write(game);
            getline(input, line);
        } else if (CMD_IS("assert_goban")) {
            /*
             * Wait for a goban as input (same format as above)
             * compare it to the current goban and exit with error
             * code if and only if they differ
             */
            std::vector<Rule*> norule;
            Game tmpGame(norule);
            reader.read_file(tmpGame, input);
            getline(input, line);
            std::cout << "here is the expected goban:" << std::endl;
            writer.write(tmpGame);
            std::cout << "here is the actual goban:" << std::endl;
            writer.write(game);
            if (tmpGame.goban.toString() != game.goban.toString()) {
                std::cout << "assertion failed" << std::endl;
                exit(EXIT_FAILURE);
            }
        } else {
            std::stringstream str(line);
            str.exceptions(std::ifstream::failbit | std::ifstream::badbit);
            try {
                unsigned int x, y;
                str >> x >> y;
                action.setMove(x - 1, y - 1);
                return action;
            } catch (std::ifstream::failure e) {
                std::cout << "invalid input, probably bad test, check it" << std::endl;
                exit(EXIT_FAILURE);
            }
        }
    }
}
