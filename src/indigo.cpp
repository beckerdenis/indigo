#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstring>
#include <string>
#include <utility>
#include <vector>
#include <tuple>
#include <ctime>

#include "indigo_version.h"
#include "configuration.h"
#include "goban.h"
#include "sdl_interface.h"
#include "command_line_interface.h"
#include "test_interface.h"
#include "human_player.h"
#include "network_player.h"
#include "app_error.h"
#include "go_error.h"
#include "action.h"
#include "random_ai_player.h"
#include "game_reader.h"
#include "text_game_reader.h"
#include "image_game_reader.h"
#include "sgf_game_reader.h"
#include "action.h"
#include "game.h"
#include "rules.h"
#include "move_tree.h"
#include "CImg.h"

#define ARG_IS(s) (strcmp(argv[cursor], s) == 0)
#define ARG_IS_OR(s1,s2) (strcmp(argv[cursor], s1) * strcmp(argv[cursor], s2) == 0)

void print_help(char *progname)
{
    std::cout << "Usage: " << progname << " [ARGS...]" << std::endl;
    std::cout << std::endl;
    std::cout << "  -s/--size W H Specify the goban size" << std::endl;
    std::cout << "                Example: '--size 13 13' or '-s 19 19' (default size)" << std::endl;
    std::cout << "  -c/--cli      Use the command-line interface" << std::endl;
//    std::cout << "  -x/--handicap Specify the handicap against the white player" << std::endl;
//    std::cout << "                Example: '-x 5' means black will have 5 initial stones" << std::endl;
//    std::cout << "                This option is available only for 13x13 and 19x19" << std::endl;
//    std::cout << "                On other sizes, handicap can be set up manually with" << std::endl;
//    std::cout << "                a combination of black moves and white passes" << std::endl;
    std::cout << "  --AI NAME     Play against the AI specified" << std::endl;
    std::cout << "                List of available AIs: random" << std::endl;
    std::cout << "  -h/--help     Print this help and exit" << std::endl;
    std::cout << "  -v/--version  Print version and exit" << std::endl;
    std::cout << "  -i/--init     Read the initial goban from standard input (see below)" << std::endl;
    std::cout << "  -f/--file F   Read the initial goban from a file (see below)" << std::endl;
    std::cout << "  --server      [Network] This instance will be the host of a network play," << std::endl;
    std::cout << "                waiting for an opponent" << std::endl;
    std::cout << "  --connect IP  [Network] This instance will connect to the given IP (or hostname)" << std::endl;
    std::cout << "                provided that the target machine has run a server instance" << std::endl;
    std::cout << "  --port PORT   [Network] Specify the port number used" << std::endl;
    std::cout << "                Example: '--port 9999'" << std::endl;
    std::cout << "  -t/--test F   [Debug] Run in test mode with the given file" << std::endl;
    std::cout << "                Example: '-t command_file.txt'" << std::endl;
    std::cout << "                The file 'command_file.txt' must contain a list of input" << std::endl;
    std::cout << "                (initial goban followed by command-line interface commands" << std::endl;
    std::cout << "                and finishing with 'exit'" << std::endl;
    std::cout << "  -r/--rules R  Choose the set of rules" << std::endl;
    std::cout << "                List of available rules: japanese (default)" << std::endl;
    std::cout << std::endl;
    std::cout << "Goban format" << std::endl;
    std::cout << "************" << std::endl;
    std::cout << std::endl;
    std::cout << "The goban specified as input (-f) can be a text file (see below), an SGF file, or" << std::endl;
    std::cout << "an image file containing the full goban." << std::endl;
    std::cout << "Since this feature is based on OpenCV, only images from OpenCV are supported." << std::endl;
    std::cout << "When a goban is asked (either from a file or from standard input)" << std::endl;
    std::cout << "the expected format is the following:" << std::endl;
    std::cout << std::endl;
    std::cout << "WIDTH HEIGHT" << std::endl;
    std::cout << "SUCCESSION_OF_TOKEN('.': empty, 'B'/'b': black, 'W'/'w': white)" << std::endl;
    std::cout << std::endl;
    std::cout << "Example:" << std::endl;
    std::cout << "5 5" << std::endl;
    std::cout << ". . w . ." << std::endl;
    std::cout << ". w b b ." << std::endl;
    std::cout << ". . w . ." << std::endl;
    std::cout << ". . b b ." << std::endl;
    std::cout << ". . w . ." << std::endl;
}

void print_version()
{
    std::cout << "IndiGo version " << INDIGO_VERSION_MAJOR << "." << INDIGO_VERSION_MINOR << std::endl;
    std::cout << "Copyright (C) 2015 Denis Becker" << std::endl;
}

void handle_arguments(int argc, char *argv[], Configuration & config)
{
    int cursor = 1;
    while (cursor < argc) {
        if (ARG_IS_OR("-h", "--help")) {
            print_help(argv[0]);
            exit(EXIT_SUCCESS);
        } else if (ARG_IS_OR("-v", "--version")) {
            print_version();
            exit(EXIT_SUCCESS);
        } else if (ARG_IS_OR("-r", "--rules")) {
            cursor++;
            if (cursor < argc) {
                config.setRules(argv[cursor]);
            } else {
                std::cerr << "Expected rule name after option '-r' or '--rules'." << std::endl;
                exit(EXIT_FAILURE);
            }
        } else if (ARG_IS_OR("-i", "--init")) {
            config.setInitialFromStdin(true);
        } else if (ARG_IS_OR("-f", "--file")) {
            cursor++;
            if (cursor < argc) {
                config.setInitialFileName(argv[cursor]);
            } else {
                std::cerr << "Expected file name after option '-f' or '--file'." << std::endl;
                exit(EXIT_FAILURE);
            }
        } else if (ARG_IS_OR("-c", "--cli")) {
            config.setCli(true);
        } else if (ARG_IS_OR("-s", "--size")) {
            if (cursor + 2 < argc) {
                size_t w = atoi(argv[cursor + 1]);
                size_t h = atoi(argv[cursor + 2]);
                config.setGobanSize(w, h);
            } else {
                std::cerr << "Expected goban size after option '-s' or '--size'." << std::endl;
                exit(EXIT_FAILURE);
            }
            cursor += 2;
        } else if (ARG_IS_OR("-t", "--test")) {
            cursor++;
            if (cursor < argc) {
                config.setTestModeWithFile(argv[cursor]);
            } else {
                std::cerr << "Expected file name after option '-t' or '--test'." << std::endl;
                exit(EXIT_FAILURE);
            }
        } else if (ARG_IS("--server")) {
            config.setServerMode();
        } else if (ARG_IS("--connect")) {
            cursor++;
            if (cursor < argc) {
                config.setClientMode(argv[cursor]);
            } else {
                std::cerr << "Expected IP address (or hostname) after option '--connect'." << std::endl;
                exit(EXIT_FAILURE);
            }
        } else if (ARG_IS("--port")) {
            cursor++;
            if (cursor < argc) {
                config.setPortNumber(atoi(argv[cursor]));
            } else {
                std::cerr << "Expected port number after option '--port'." << std::endl;
                exit(EXIT_FAILURE);
            }
        } else if (ARG_IS("--AI")) {
            cursor++;
            if (cursor < argc) {
                config.setAI(argv[cursor]);
            } else {
                std::cerr << "Expected AI name after option '--AI'." << std::endl;
                exit(EXIT_FAILURE);
            }
        } else {
            std::cerr << "Unrecognized argument: '" << argv[cursor] << "'." << std::endl;
            print_help(argv[0]);
            exit(EXIT_FAILURE);
        }
        cursor++;
    }
}

void alert(AppError & e)
{
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error", e.what(), nullptr);
    std::cerr << e.what() << std::endl << e.getLibMsg() << std::endl;
}

Interface * createInterface(Configuration & config, Game & game)
{
    if (config.isTestMode()) {
        return new TestInterface(game, config.getInitialFileName());
    } else if (config.isCli()) {
        return new CommandLineInterface(game);
    } else {
        return new SDLInterface(game);
    }
}

std::tuple<Player*, Player*> createPlayers(Configuration & config, Interface * interface, Game & game)
{
    Player * black;
    Player * white;
    if (config.isClientMode()) {
        black = new NetworkPlayer(Player::Color::BLACK, config.getTargetIP(), config.getPortNumber());
    } else {
        black = new HumanPlayer(Player::Color::BLACK, interface);
    }
    if (config.isServerMode()) {
        white = new NetworkPlayer(Player::Color::WHITE, config.getPortNumber());
    } else if (config.isVersusAI()) {
        if (config.getNameAI() == "random") {
            white = new RandomAIPlayer(Player::Color::WHITE, game);
        } else {
            throw AppError("AI named '" + config.getNameAI() + "' doesn't exist.");
        }
    } else {
        white = new HumanPlayer(Player::Color::WHITE, interface);
    }
    return std::make_tuple(black, white);
}

GameReader * createGameReader(Configuration & config)
{
    const std::string & infile = config.getInitialFileName();
    std::string ext = infile.substr(infile.find_last_of(".") + 1);
    if (ext == "txt") {
        return new TextGameReader(infile);
    } else if (ext == "sgf") {
        return new SGFGameReader(infile);
    }
    try {
        return new ImageGameReader(infile);
    } catch (cimg_library::CImgIOException & e) {
        throw AppError("Unhandled input file format: " + ext);
    }
}

void createRules(Configuration & config, std::vector<Rule*> & rules)
{
    if (config.getRules() == "japanese") {
        rules.push_back(new CaptureRule());
        rules.push_back(new NoSuicideRule());
        rules.push_back(new SimpleKoRule());
    } else {
        throw AppError("Unhandled rule set: " + config.getRules());
    }
}

int main(int argc, char *argv[])
{
    srand(time(NULL));

    Configuration config;
    handle_arguments(argc, argv, config);

    /*
     * Creation of the set of rules.
     * This defines what is allowed and what's not allowed to play.
     */
    std::vector<Rule*> rules;
    try {
        createRules(config, rules);
    } catch (AppError & e) {
        alert(e);
        return EXIT_FAILURE;
    }

    /*
     * Creation of the game.
     */
    Game game(rules);

    /* 
     * First case is for reading games (from a file, see help).
     * Second case is for an empty game.
     */
    game.setGobanSize(config.getGobanWidth(), config.getGobanHeight());
    if (config.isInitialFileSpecified()) {
        try {
            GameReader * game_reader = createGameReader(config);
            game_reader->read(game);
            delete game_reader;
        } catch (AppError & e) {
            alert(e);
        }
    }

    /*
     * Creation of the interface.
     * This defines how the human in front of its computer sends input to indigo.
     * Can be console interface, or graphical for example.
     */
    Interface * interface;
    try {
        interface = createInterface(config, game);
    } catch (AppError & e) {
        alert(e);
        // doesn't throw any excpetion
        interface = new CommandLineInterface(game);
    }

    /*
     * Creation of players.
     * This defines how the players compute their move. For exemple, just wait
     * for input for humans, or compute it for computer players.
     */
    Player * black;
    Player * white;
    try {
        std::tie(black, white) = createPlayers(config, interface, game);
    } catch (AppError & e) {
        alert(e);
        delete interface;
        return EXIT_FAILURE;
    }

    // main loop

    interface->start();
    game.start(black, white);

    while (!game.isOver()) {
        Action action = game.getCurrentPlayer().nextAction();
        if (!(action.isGhostMove() || action.isNothing())) {
            try {
                game.execute(action);
            } catch (GoError & e) {
                // nothing for now
            }
        }
        
        interface->update(action, game.getCurrentPlayer());
    }

    /*
     * Cleaning stuff.
     */

    for (auto & rule : rules) {
        delete rule;
    }
    delete black;
    delete white;
    delete interface;

    return EXIT_SUCCESS;
}
