#include <iostream>

#include "configuration.h"
#include "app_error.h"
#include "goban.h"
#include "text_game_reader.h"

Configuration::Configuration() :
    rules("japanese"),
    initialFileName(""),
    initialFileSpecified(false),
    initialFromStdin(false),
    testMode(false),
    cli(false),
    gbwidth(19),
    gbheight(19),
    serverMode(false),
    clientMode(false),
    targetIP(""),
    portNumber(9999),
    nameAI(""),
    versusAI(false)
{
}

void Configuration::setRules(char * ruleset)
{
    rules = std::string(ruleset);
    if (rules != "japanese") {
        std::cerr << "Undefined rules '" << rules << "', default to japanese." << std::endl;
        rules = "japanese";
    }
}

void Configuration::setInitialFileName(char * filename)
{
    initialFileName = std::string(filename);
    initialFileSpecified = true;
}

void Configuration::setCli(bool value)
{
    cli = value;
}

void Configuration::setGobanSize(size_t width, size_t height)
{
    if (width < 1) {
        width = 1;
        std::cerr << "Width out of bounds [1;" << Goban::MAX_WIDTH << "], minimum used (1)." << std::endl;
    } else if (width > Goban::MAX_WIDTH) {
        width = Goban::MAX_WIDTH;
        std::cerr << "Width out of bounds [1;" << Goban::MAX_WIDTH << "], maximum used (" << Goban::MAX_WIDTH << ")." << std::endl;
    }
    if (height < 1) {
        height = 1;
        std::cerr << "Height out of bounds [1;" << Goban::MAX_HEIGHT << "], minimum used (1)." << std::endl;
    } else if (height > Goban::MAX_HEIGHT) {
        height = Goban::MAX_HEIGHT;
        std::cerr << "Height out of bounds [1;" << Goban::MAX_HEIGHT << "], maximum used (" << Goban::MAX_HEIGHT << ")." << std::endl;
    }
    gbwidth = width;
    gbheight = height;
}

void Configuration::setInitialFromStdin(bool value)
{
    initialFromStdin = value;
}

void Configuration::setTestModeWithFile(char * fileName)
{
    testMode = true;
    setInitialFileName(fileName);
}

void Configuration::setServerMode()
{
    serverMode = true;
    clientMode = false;
}

void Configuration::setClientMode(char * ip)
{
    serverMode = false;
    clientMode = true;
    targetIP = std::string(ip);
}

void Configuration::setPortNumber(int value)
{
    if (value > 0 && value < 65536) {
        portNumber = static_cast<uint16_t>(value);
    }
}

void Configuration::setAI(char * name)
{
    nameAI = std::string(name);
    versusAI = true;
}
