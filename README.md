# IndiGo

## Dependencies

Indigo has the following dependencies. Here is the list:

- SDL2
- CImg (automatically downloaded at compile time if an internet connection is available)

## Build Project

Here is how to try indigo:

    mkdir build # create seperate dir to build the project (optional)
    cd build    # go in build dir (optional)
    cmake ..    # run cmake (if not in build directory, just run 'cmake')
    make        # make executable
    ./indigo    # try it

## Example of Image Reading

One can load an initial configuration from an image file. This is quite experimental,
but works for images in good conditions. An example file that works well can be found
in 'test/game.png'. To load this example, run the following command (after the
procedure of the previous section):

    ./indigo -f ../test/game.png
