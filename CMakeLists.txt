cmake_minimum_required(VERSION 2.8)

include(ExternalProject)

project(indigo)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

# Configuration

set(INDIGO_VERSION_MAJOR 0)
set(INDIGO_VERSION_MINOR 3)
set(PROJECT_SOURCE_DIR "src")
set(PROJECT_INCLUDE_DIR "include")
set(PROJECT_TEMPLATE_DIR "template")
set(INCLUDE_DIRS ${PROJECT_INCLUDE_DIR} "${CMAKE_BINARY_DIR}/include")
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/Modules/")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Werror -pedantic -std=c++11")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -O0 -g")
set(CMAKE_BUILD_TYPE "Debug")

# Dependencies

find_package(SDL2 REQUIRED)
set(INCLUDE_DIRS ${INCLUDE_DIRS} ${SDL2_INCLUDE_DIR})
if (WIN32)
    set(LIBS ${LIBS} ${SDL2_LIBRARY})
else()
    set(LIBS ${LIBS} ${SDL2_LIBRARY} X11)
endif()

find_package(SDL2_image REQUIRED)
set(INCLUDE_DIRS ${INCLUDE_DIRS} ${SDL2_IMAGE_INCLUDE_DIRS})
set(LIBS ${LIBS} ${SDL2_IMAGE_LIBRARIES})

find_package(SDL2_net REQUIRED)
set(INCLUDE_DIRS ${INCLUDE_DIRS} ${SDL2_NET_INCLUDE_DIRS})
set(LIBS ${LIBS} ${SDL2_NET_LIBRARIES})

find_package(SDL2_gfx REQUIRED)
set(INCLUDE_DIRS ${INCLUDE_DIRS} ${SDL2_GFX_INCLUDE_DIRS})
set(LIBS ${LIBS} ${SDL2_GFX_LIBRARIES})

# External Projects

set_property(DIRECTORY PROPERTY EP_BASE ${CMAKE_SOURCE_DIR}/external)

#     CImg (light header file to read multiple image formats)

externalproject_add(cimg
    URL http://cimg.eu/files/CImg_latest.zip
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ""
    INSTALL_COMMAND ""
)
externalproject_get_property(cimg SOURCE_DIR)
set(INCLUDE_DIRS ${INCLUDE_DIRS} ${SOURCE_DIR})
set(LIBS ${LIBS})

#     SGFC (reference parser implementation for SGF Go format)

externalproject_add(sgfc
    URL http://www.red-bean.com/sgf/sgfc/sgfc-1-17.tar.gz
    CONFIGURE_COMMAND ""
    BUILD_COMMAND gcc -w -DVERSION_NO_MAIN -O1 -c <SOURCE_DIR>/parse2.c <SOURCE_DIR>/execute.c
        <SOURCE_DIR>/gameinfo.c <SOURCE_DIR>/load.c <SOURCE_DIR>/main.c <SOURCE_DIR>/parse.c
        <SOURCE_DIR>/properties.c <SOURCE_DIR>/save.c <SOURCE_DIR>/strict.c <SOURCE_DIR>/util.c
    INSTALL_COMMAND ar -cq <INSTALL_DIR>/libsgfc.a <BINARY_DIR>/parse2.o <BINARY_DIR>/execute.o
        <BINARY_DIR>/gameinfo.o <BINARY_DIR>/load.o <BINARY_DIR>/main.o <BINARY_DIR>/parse.o
        <BINARY_DIR>/properties.o <BINARY_DIR>/save.o <BINARY_DIR>/strict.o <BINARY_DIR>/util.o
)
externalproject_get_property(sgfc SOURCE_DIR)
externalproject_get_property(sgfc INSTALL_DIR)
set(INCLUDE_DIRS ${INCLUDE_DIRS} ${SOURCE_DIR})
set(LIBS ${LIBS} ${INSTALL_DIR}/libsgfc.a)

# Compilation & link

configure_file(
    ${PROJECT_TEMPLATE_DIR}/indigo_version.h.in
    ${PROJECT_INCLUDE_DIR}/indigo_version.h
)

include_directories(${INCLUDE_DIRS})

add_executable(indigo
    ${PROJECT_SOURCE_DIR}/indigo.cpp
    ${PROJECT_SOURCE_DIR}/configuration.cpp
    ${PROJECT_SOURCE_DIR}/goban_intersection.cpp
    ${PROJECT_SOURCE_DIR}/goban.cpp
    ${PROJECT_SOURCE_DIR}/test_interface.cpp
    ${PROJECT_SOURCE_DIR}/command_line_interface.cpp
    ${PROJECT_SOURCE_DIR}/sdl_interface.cpp
    ${PROJECT_SOURCE_DIR}/human_player.cpp
    ${PROJECT_SOURCE_DIR}/network_player.cpp
    ${PROJECT_SOURCE_DIR}/random_ai_player.cpp
    ${PROJECT_SOURCE_DIR}/game.cpp
    ${PROJECT_SOURCE_DIR}/move_tree.cpp
    ${PROJECT_SOURCE_DIR}/rules.cpp
    ${PROJECT_SOURCE_DIR}/text_game_reader.cpp
    ${PROJECT_SOURCE_DIR}/sgf_game_reader.cpp
    ${PROJECT_SOURCE_DIR}/image_game_reader.cpp
    ${PROJECT_SOURCE_DIR}/text_game_writer.cpp
)

add_dependencies(indigo cimg sgfc)

target_link_libraries(indigo ${LIBS})

enable_testing()
add_subdirectory(test)

# Additional files

# Don't copy if build is in source dir
if (NOT CMAKE_SOURCE_DIR STREQUAL CMAKE_BINARY_DIR)
    set(RESOURCES img)
    foreach(item IN LISTS RESOURCES)
        message(STATUS "Copying resource ${item}")
        file(COPY ${item} DESTINATION .)
    endforeach()
endif()
